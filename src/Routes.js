import {StyleSheet} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import React from 'react';

import Login from './Components/Initial/Login';
import Cadastro from './Components/Initial/Cadastro';
import NovaSenha from './Components/Initial/NovaSenha';
import Home from './Components/Home';
import ChavePB from './Components/Initial/ChavePB';
import Autentica from './Components/AutenticaUsuario';
import ExamplePage from './Components/ExamplePage';
import Contatos from './Components/Contatos';
import Conversas from './Components/Conversas';

import Inicio from './Components/Initial/Initial';
import Nome from './Components/SignUp/Nome';
import EmailPage from './Components/SignUp/Email';
import PassPage from './Components/SignUp/Password';
import PassKeyPage from './Components/SignUp/PassKey';
import SignIn from './Components/SignIn/SignIn';
import Search from './Components/SearchContact';
import ChaveSecret from './Components/SignIn/ChaveSecret';
import Profile from './Components/Profile';
import Username from './Components/SignUp/Username';
import ProfileSearch from './Components/Search/ProfileSearch';
import Detalhes from './Components/Search/Detalhes';
import Configuracao from './Components/Configuracao';
import Pendencias from './Components/Contatos/Pendencias';
import Chat from './Components/Chat'
import PlayMusic from './Components/PlayMusic'
export default class Routes extends  React.Component{
  constructor(props){
    super(props);
    this.state = { items: this.props.item }
   // alert('Item count on componentWillReceiveProps:', this.state.items);
  }

    render(){
        return (
            <Router   titleStyle={styles.textAll}>
                 <Scene
          key="root"
          component={Autentica}
        >

 
         <Scene key='Autentica' component={Autentica} title="Autentica" hideNavBar={true} />
        <Scene key='Home' component={Home} title="Home" hideNavBar={true} />
        <Scene key='Contatos' component={Contatos} title='Contatos' hideNavBar={true} />
        <Scene key='Conversas' component={Conversas} title='Conversas' hideNavBar={true} />
        <Scene key='Login' component={Login} title='Login' hideNavBar={true} />
        <Scene key='Cadastro' component={Cadastro} title="Cadastro" hideNavBar={true} />
        <Scene key='NovaSenha' component={NovaSenha} title="NovaSenha" hideNavBar={true} />
        <Scene key='ChavePB' component={ChavePB} title="ChavePB" hideNavBar={true} />
        <Scene key='ExamplePage' component={ExamplePage} title="ExamplePage" hideNavBar={true} />
        <Scene key='PlayMusic' component={PlayMusic} title="PlayMusic" hideNavBar={true} /> 
      
        <Scene key='Inicio' component={Inicio} title="Inicio" hideNavBar={true} />
        <Scene key='Nome' component={Nome} title="Nome" hideNavBar={true} />
        <Scene key='EmailPage' component={EmailPage} title="EmailPage" hideNavBar={true} />
        <Scene key='PassPage' component={PassPage} title="PassPage" hideNavBar={true} />
        <Scene key='PassKeyPage' component={PassKeyPage} title="PassKeyPage" hideNavBar={true} />
        <Scene key='SignIn' component={SignIn} title="SignIn" hideNavBar={true} />
        <Scene key='Search' component={Search} title="Search" hideNavBar={true} />
        <Scene key='ChaveSecret' component={ChaveSecret} title="ChaveSecret" hideNavBar={true} />
      
        <Scene key='Username' component={Username} title="Username" hideNavBar={true} />
        <Scene key='ProfileSearch' component={ProfileSearch} title="ProfileSearch" hideNavBar={true} />
        <Scene key='Detalhes' component={Detalhes} title="Detalhes" hideNavBar={true} />
        <Scene key='Configuracao' component={Configuracao} title="Configuracao" hideNavBar={true} />
        <Scene key='Pendencias' navigationBarStyle={styles.all} component={Pendencias} title="Solicitações" hideNavBar={false} />
        <Scene key='Profile' component={Profile} title="Profile" hideNavBar={true} />
        <Scene key='Chat' component={Chat} title="Chat" hideNavBar={true} />
        
        </Scene>

            </Router>
        );
    }
}




const styles = StyleSheet.create({
    all:{
      backgroundColor:'green'
    },
    textAll:{
      color:'#fff'
    },
    navBar: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'red', // changing navbar color
    }
  })
