import firebase from 'firebase';
import {VIEW_PROFILE,DADOS_PRIVACIDADE,USER_CONTATOS,USER_FETCH_SOLICITACOES,LOAD_CHANGE_PRIVACIDADE,LOAD_PRIVACIDADE,FECHAR_MODAL,BUSCANDO_USER,USER_SEARCH, MODIFICAR_LOAD,CADASTRO_USUARIO,ERRO_CADASTRO,LOGGED, LOGIN, MODIFICAR_EMAIL, MODIFICAR_SENHA, ALT_EMAIL_LOGIN, ALT_SENHA_LOGIN} from './types';
import { Actions } from 'react-native-router-flux';
import b64 from 'base-64';
import _ from 'lodash'
import {AsyncStorage} from 'react-native';
import {Crypt, keyManager, RSA} from 'hybrid-crypto-js';
import ReactAES from 'react-native-aes-encryption';



export const alterarLoad = (val) => {
    return{
        type:MODIFICAR_LOAD,
        payload:val
    }
}
export const verificaLoad = (nome) =>{
    return dispatch => {
        if(nome.length == 0){
            dispatch({type:BUSCANDO_USER, loadingSearchuser:false})
            dispatch({type:USER_SEARCH, users_encontrados:""})
        }
    }
}

export const searchUser = (nome) => {


    return dispatch =>{

        dispatch({type:BUSCANDO_USER, loadingSearchuser:true})
        firebase.database().ref('PROFILE_USER/')
        .orderByChild("nick")
        .startAt(nome)
        .endAt(nome)
        .on("child_added", function(data) {
                let tamanho = data.val();
                if(tamanho.length == 0){
                    alert('vazio');
                }else{
                    dispatch({type:BUSCANDO_USER, loadingSearchuser:false})
                    dispatch({type:USER_SEARCH, users_encontrados:data.val()})
                }
        });
 }

}


/*----------------------------------------------- PROFILE -------------------------------------*/

// EFETUAR A CONTAGEM DE VISUALIZACAO DOS PERFIS

export const visualizarUserContagem = (emailuser,myemail) => {
    return dispatch => {
       
        firebase.database().ref(`/PRIVACIDADE_USERS/${b64.encode(myemail)}`).once('value').then(snapshot => {
            if(snapshot.val().viewProfile){
                firebase.database().ref(`/PRIVACIDADE_USERS/${emailuser}`).once('value').then(snapshots => {
                    if(snapshots.val().viewProfile){
                        if(usrAtual == false){                                    
                            let qnt = 1;
                            var postData = {
                                quantidade:1,
                                email:b64.encode(myemail)
                
                              };                             
                             firebase.database().ref(`/COUNT_VIEWS/${email}/${user}`).once('value').then(function(snapshot) {
                
                            if(snapshot.val() == null){
                                firebase.database().ref(`/COUNT_VIEWS/${emailuser}/${myemail}`).set(postData).then(sucess =>{
                                 
                                }).catch(erro => {
                                    alert('nao salvou: '+erro.message)
                                })                
                            }else{                
                                let val = _.values(snapshot.val());                
                                efetuarContagem(emailuser, myemail , snapshot.val().quantidade);
                            }
                
                        })
                        }
                    }
                })
               
            }
        }).catch(erro =>{

        })
       
    }
}

const efetuarContagem = (emailuser, myemail, val )=>{


  
   let final;
   final = parseInt(val);
   var dados = {
        quantidade: final+1,
        email:b64.encode(myemail)
    }

    firebase.database().ref(`/COUNT_VIEWS/${emailuser}/${myemail}`).set(dados).then(sucess =>{

    }).catch(erro => {
        alert('nao salvou: '+erro.message)
    })
}

export const AdicionarUsuario = ({id,myemail, email, foto, nome}) =>{

  return dispatch => {
    firebase.database().ref(`/CONTATOS/${b64.encode(myemail)}/${email}`).set({ 
        email:email, 
        accept:false,
        pic: foto,
        status:'',
        
        name:nome}).then(sucesso => {
        
            var playersRef = firebase.database().ref('PROFILE_USER/');

            playersRef.orderByChild("email").startAt(b64.encode(myemail)).endAt(b64.encode(myemail)).on("child_added", function(data) {
             
                        firebase.database().ref(`/CONTATOS/${email}/${b64.encode(myemail)}`).set({ 
                            email:b64.encode(myemail), 
                            accept:false,
                            pic:data.val().pic,
                            status:'',                            
                            nome:data.val().name
                        }).then(sucesso => {
    
                        firebase.database().ref(`/SOLICITACAO_USER/${email}/${b64.encode(myemail)}`).set({
                            emailSolicitante:b64.encode(myemail),
                            nome:data.val().nome,
                            nick:data.val().nick,
                            idProfile:data.val().idProfile,
                            pic:data.val().pic,
                            accept:false
                        }).then(sucesso => {
                            firebase.database().ref(`/SOLICITACOES_ENVIADAS/${b64.encode(myemail)}/${email}`).set({
                                emailPendente:email,
                                pic:foto,
                                nome:nome,
                                accept:false
                            }).then(sucesso => {
                                dispatch({type:"adicionou",payload:true});
                            }).cath(erro =>{
                                dispatch({type:"adicionou",payload:erro.message});
                            })
    
                        }).catch(erro => {
                            dispatch({type:"adicionou",payload:erro.message});
                        })
                
          }) 
          
        })
        .catch(erro =>{

        })

    

    }).catch(erro => {
        dispatch({type:"adicionou",payload:erro.message});
    })}
}

export const verificaContato = (emailuser,myemail) => {
    email,meucontato
  
  
  if(emailuser === b64.encode(myemail)){
    return {
      type:'verifica',
      usuario_final:true
    }
  }else{   
    return dispatch => {
        dispatch({type:'verifica',usuario_final:false})
      
        firebase.database().ref(`/CONTATOS/${ b64.encode(myemail)}/${emailuser}`)
        .orderByChild("accept")
        .equalTo(true).on('value',function(data){
            let valor = data.val();
            if(valor == null){
                dispatch({type:'meucontato',
                payload:false})
            }else{
                dispatch({type:'meucontato',
                payload:true})
            }
            
        })

    }

  }

}


/*----------------------------------------------- FIM PROFILE -------------------------------------*/




/*----------------------------------------------- PRIVACIDADE -------------------------------------*/



export const privacidateUser = (myemail) => {
 
 return dispatch =>{
  
  dispatch({type:LOAD_PRIVACIDADE, load_privacidade:true})
  firebase.database().ref(`/PRIVACIDADE_USERS/${b64.encode(myemail)}`).once('value').then(snapshot => {
    dispatch({type:LOAD_PRIVACIDADE, load_privacidade:false})
    dispatch({
        type:DADOS_PRIVACIDADE,
        
        viewProfile:snapshot.val().viewProfile,
        viewLastMusic:snapshot.val().viewLastMusic,
        viewScreenShot:snapshot.val().viewScreenShot,
        viewStatusOn:snapshot.val().viewStatusOn
    })
  }).catch(erro =>{
    dispatch({type:LOAD_PRIVACIDADE, load_privacidade:false})
      alert(erro.message)
  })
 }

}

export const changeViewProfile = (val,myemail) => {
 
    return dispatch =>{        
        dispatch({type:DADOS_PRIVACIDADE,  viewProfile:val,})
        firebase.database().ref(`PRIVACIDADE_USERS/${b64.encode(myemail)}`).update({            
            viewProfile:val
        }).catch(erro => {
            alert('falou')
        })


    }
}


/*----------------------------------------------- FIM PRIVACIDADE -------------------------------------*/


/*----------------------------------------------- CONTATOS -------------------------------------*/

export const fecthUserRecebidos = (myemail) => {
   
    return dispatch =>{
       
                firebase.database().ref(`/SOLICITACAO_USER/${b64.encode(myemail)}`)
                .orderByChild("accept")
                .equalTo(false).on("value", function(data) {                                     
                    dispatch({type:USER_FETCH_SOLICITACOES, payload:data.val()});
                })
    
    }    
}

export const aceitarDeletarUser = (val, condicion ,myemail) => {
    let crypt = new Crypt();
  
   
    return dispatch =>{
        
        if(condicion){
            let keys = { key_first:generate(val),key_second:generate(b64.encode(myemail))};
          
            firebase.database().ref(`/USERS/${val}`).once('value').then(snapshot =>{
                firebase.database().ref(`/USERS/${b64.encode(myemail)}`).once('value').then(snapshot2 =>{
                                        
                    var user1 = b64.encode(crypt.encrypt(snapshot.val().publicKey,JSON.stringify(keys)));
                    var user2 =  b64.encode(crypt.encrypt(snapshot2.val().publicKey,  JSON.stringify(keys)));
                    let idKey = generate(1);
                 
                    firebase.database().ref(`/SOLICITACAO_USER/${b64.encode(myemail)}/${val}`).update({
                        accept:true
                    }).then(sucesso => {
                        firebase.database().ref(`/SOLICITACOES_ENVIADAS/${val}/${b64.encode(myemail)}`).update({
                            accept:true
                        }).then(sucesso => {
                           
                            firebase.database().ref(`/CONTATOS/${b64.encode(myemail)}/${val}`)
                            .update({ 
                                 accept:true,
                                 keysMessage:user2,
                                 idKey
                            
                            }).then(sucesso => {
                                firebase.database().ref(`/CONTATOS/${val}/${b64.encode(myemail)}`).update({  
                                    accept:true,
                                    keysMessage:user1,
                                    idKey
                                }).then(sucesso => {
                                    alert('aceitou')
                                })
                            })
                                //
                        }).catch(erro => {
                            alert('falha ao aceitar 1: '+erro.message)
                        })
                    }).catch(erro => {
                        alert('falha ao aceitar 2')
                    })  
                }).catch(erro => {
                    alert(erro.message)
                })
            }).catch(erro => {
                alert(erro.message)
            })
            
           
        }else{
    
        }
    }
}

const generate = (valor) =>{
    
    let val = Math.floor((Math.random() * 6000) * 2018 * 2017);
    return b64.encode(val+valor);
}
export const fetchMyContacts = (email) => {

    return dispatch =>{
        
        firebase.database().ref(`/CONTATOS/${b64.encode(email)}`)
        .orderByChild("accept")
        .equalTo(true).on("value", function(data) {                  
           
         
            dispatch({type:USER_CONTATOS, payload:(data.val())});
        })
    
    }
}

export const chaveContatos = ({myPrivateKey,  publickeyUser }) => {
    let crypt = new Crypt();
    return dispatch =>{
      
       var dados =  crypt.decrypt( b64.decode(myPrivateKey),b64.decode(publickeyUser) )
        var pass =    JSON.parse(dados.message)
            dispatch({type:'TO/MESSAGE', keysMessage:pass.key_first })           
                  
     
       
    }
}

export const mensagemEnviar = (val) => {
    return{
        type:'MESSAGE/SEND',
        payload:val
    }
}

export const mensagemContato = ({myEmail, message, touser,keysMessage,idKey}) => {
    return dispatch => {
      
       ReactAES.encrypt(message, b64.encode(keysMessage), b64.encode(idKey)).then(resp => {
            firebase.database().ref(`/MESSAGES/${b64.encode(myEmail)}/${touser}`).push({
                message:resp,
                type:'send'
            }).then(success => {
                firebase.database().ref(`/MESSAGES/${touser}/${b64.encode(myEmail)}`).push({
                    message:resp,
                    type:'receive'
                }).then(sucesso => {
                    // MENSAGEM ENVIADA
                    alert('mensagem enviada')
                }).then(cabe => {
                    // ADICIONAR MEU CABECALHO
                    var playersRef = firebase.database().ref(`/PROFILE_USER/`);
                        
                    playersRef.orderByChild("emailCrip").equalTo(touser).on("child_added", function(data) {           
                    
                      firebase.database().ref(`/MESSAGES_USERS/${b64.encode(myEmail)}/${touser}`)
                      
                      .set({
                        nome:data.val().nick,
                        email:touser, 
                        foto: data.val().pic
                    }).then
                    });

                  
                }).then(prox => {
                      var playersRef = firebase.database().ref(`/PROFILE_USER/`);
                        
                        playersRef.orderByChild("emailCrip").equalTo(b64.encode(myEmail)).on("child_added", function(data) {           
                        
                          firebase.database().ref(`/MESSAGES_USERS/${touser}/${b64.encode(myEmail)}`)
                        .set({
                            nome:data.val().nick,
                            email:b64.encode(myEmail), 
                            foto: data.val().pic
                        })
                        });

                  
                })
            }).catch(error => {
                alert(error.message)
            })
        }).catch(error => {
            alert(error.message)
        })  
      
    }
}

export const loadMessages = ({}) => {

}
export const loadContatoMessages = (email) => {
    
    return dispatch => {
        firebase.database().ref(`/MESSAGES_USERS/${emaill}`)
        .on("value", snapshot =>{
            dispatch({ type:LISTAR_USUARIOS_ALL_CONVERSAS, payload: snapshot.val() });
        })
    }
}
/*----------------------------------------------- FIM CONTATOS -------------------------------------*/


export const loggout = (email) =>{
    return dispatch =>{

        
        let emailCrip = b64.encode(email);
        firebase.database().ref(`/STATUS_USER/${emailCrip}`)
                    .set({
                        userStatus:false
                    }).then(sucesso => {
                        firebase.auth().signOut().then(function() {
                            
                            
                            dispatch({type:LOGGED, payload:false, statusUser:false});
                            dispatch({type:'LOGGOUT/SAIR'});
                            Actions.Autentica();
                          }).catch(function(error) {
                            alert('nao foi possivel deslogar '+error.message);
                          });
                    })


    }
}



