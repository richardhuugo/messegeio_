import firebase from 'firebase';
import {CADASTRO_USUARIO,
    ERRO_CADASTRO,
    NOME_CADASTRO,
    FECHAR_MODAL,
    LOGGED,
    SENHA_PUBLICA_USUARIO,
    LOGIN,
    MODIFICAR_EMAIL,
    MODIFICAR_SENHA,
    USER_NAME_CADASTRO,
    VERIFICA_DISPONIBILIDADE,
    MODIFICAR_PIN,
    ALT_EMAIL_LOGIN,
    ALT_SENHA_LOGIN
    } from './types';
import { Actions } from 'react-native-router-flux';
import b64 from 'base-64';
import _ from 'lodash';
import firestore from 'firebase/firestore';
import ReactAES from 'react-native-aes-encryption';
import {Crypt, keyManager, RSA} from 'hybrid-crypto-js';


export const cadastroUserGoogle = () => {


    return dispatch =>{
        dispatch({type:CADASTRO_USUARIO, load:true});

    }

}

export const verificaUsuario = () => {
    return dispatch => {
        firebase.auth().onAuthStateChanged(function(user) {
            
            if(user){


                dispatch({type:LOGGED, payload:true});

            }else{


                dispatch({type:LOGGED, payload:false});
            }
          });
    }
}

export default autenticaSenhaPin = () => {
    
}
/*----------------------------------------------- LOGIN -------------------------------------*/

export const loginUser = ({email, senha,pin}) =>{
   
    return dispatch => {
       /*  inicio */
      
        dispatch({type:LOGIN, loadLogin:true});
        usr.signInWithEmailAndPassword(email, senha).then(sucesso =>{
           
            dispatch({type:LOGGED, payload:false});

           let keys;        
           firebase.database().ref(`/USERS/${b64.encode(email)}`)
           .once('value', snapshot => {
            keys  = _.first(_.values(snapshot.val()));//, (uid,val ) =>{                      
                parametro1 = snapshot.val().hash+snapshot.val().id

                try{
                    ReactAES.decrypt(snapshot.val().privatePass,generateKeyB64(parametro1),generateKeyB64(pin)).then(
                        result=>{
                            let dados = JSON.parse(result)                                                                            
                            firebase.database().ref(`/STATUS_USER/${b64.encode(email)}`)
                                .set({
                                    userStatus:true
                                }).then(sucesso =>{                                    
                                        dispatch({type:LOGIN, loadLogin:false});
                                        dispatch({type:'PRIVATE/KEY', privateKey:b64.encode(dados.key)});
                                        dispatch({type:LOGIN, loadLogin:false});
                                        dispatch({type:LOGGED, payload:true});                
                                        Actions.Autentica({id: snapshot.val().id  , hash: snapshot.val().hash});
                                        Actions.refresh();                                                                                                                   
                                })
                        }
                    ).catch(
                        error => {
                            dispatch({type:LOGIN, loadLogin:false});
                            alert('PIN ERRADO'+error.message);
                            firebase.auth().signOut().then(function() {
                                dispatch({type:LOGGED, payload:false, statusUser:false});
                                Actions.Autentica();
                              }).catch(function(error) {
                                alert('nao foi possivel deslogar '+error.message);
                              });
                        }
                    );
                }catch(e){
                    alert('ENTROU AQUI: '+e.message)
                    dispatch({type:LOGIN, loadLogin:false});
                    firebase.auth().signOut().then(function() {
                        dispatch({type:LOGGED, payload:false, statusUser:false});
                        Actions.Autentica();
                      }).catch(function(error) {
                        alert('nao foi possivel deslogar '+error.message);
                      });
                } 
                 
            /*
                   
              */    
            
              
               
           }).catch(erro =>{
            dispatch({type:LOGIN, loadLogin:false});
               alert(erro.message)
           })

        }).catch(erro => {
            dispatch({type:LOGIN, loadLogin:false});
            alert(erro.message);
        }) 

        /* FIM */
    }
}
export const modificarSenhaLogin = (senha) => {
    return dispatch => {
        dispatch({type:ALT_SENHA_LOGIN, payload:senha})
    }
}
export const modificarEmailLogin = (email) => {
    return dispatch => {
        dispatch({type:ALT_EMAIL_LOGIN, payload:email})
    }
}
export const modificarPin = (pin) => {
    return {
        type:MODIFICAR_PIN,
        payload:pin
    }
}



/*----------------------------------------------- CADASTRO -------------------------------------*/


/*----------------------------------------------- CADASTRO -------------------------------------*/
export const cadastroUser = ({nome_cadastro, email, senha, pin_cadastro,user_name}) => {

   return dispatch => {
        dispatch({type:CADASTRO_USUARIO, load:true});
        let rsa = new RSA();
            let idhash = generateHashID();          
            let dataCadastro = new Date().getFullYear();            
            let emailCript = generateKeyB64(email);        
    
            try{
                // Generate 1024 bit RSA key pair
                rsa.generateKeypair(function(keypair) {

                    let dados = { pin:pin_cadastro, key: keypair.privateKey}

                    ReactAES.encrypt(JSON.stringify(dados),generateKeyB64(emailCript+idhash),generateKeyB64(pin_cadastro)).then(
                        result=>{                                
                            firebase.auth().createUserWithEmailAndPassword(email, senha).then(sucesso =>{                                                                        
                                            firebase.database().ref(`/USERS/${emailCript}`)
                                            .set({
                                
                                                id:idhash,
                                                nome:nome_cadastro,
                                                email,
                                                hash:emailCript,                    
                                                dataCadastro:dataCadastro,
                                                privatePass: result,
                                                publicKey:keypair.publicKey                                                                                                                
                                            })
                                            .then(sucess =>{
                                                    firebase.database().ref(`/STATUS_USER/${emailCript}`)
                                                        .set({
                                                            userStatus:true
                                                        }).then(sucesso => {
                                                        let idProfile = generateHashID();
                                                        firebase.database().ref(`/PROFILE_USER/`)
                                                        .push({
                                                            idProfile: idProfile,
                                                            nome: nome_cadastro,
                                                            nick:user_name,
                                                            email:emailCript,
                                                            publicKey:keypair.publicKey,
                                                            pic:'https://thumbs.dreamstime.com/b/%C3%ADcone-do-sinal-do-usu%C3%A1rio-s%C3%ADmbolo-da-pessoa-avatar-humano-84519083.jpg'
                                
                                                        })
                                                                                                            
                                                    }).then(novo =>{                                                                                                                                                  
                                                            firebase.database().ref(`/PRIVACIDADE_USERS/${emailCript}`).set({
                                                                viewProfile:true,
                                                                viewScreenShot:true,
                                                                viewStatusOn:true,
                                                                viewLastMusic:true,                                                    
                                                            })
                                                            dispatch({type:LOGGED, payload:true});
                                                            dispatch({ type:CADASTRO_USUARIO, load:false});
                                                            Actions.refresh();
                                                            Actions.Autentica({nome_AUTH: nome_cadastro, idhash});
                                                            
                                                        })                                                                                            
                                            })  

                                }).catch(erro =>{
                                    alert(erro.message)
                                });


                        }
                        ).catch(
                            error => {
                                alert(error.message);
                            }
                        );                                 
                }, 1024);  // Key size
                }catch(e){
                    alert('falhou '+e.message)
            }
           
   }
}

export const verificaUsername = (username) =>{
    return dispatch =>{
        
        var playersRef = firebase.database().ref(`/PROFILE_USER/`);
        let tamanho="";
        playersRef.orderByChild("nick").equalTo(username).on("child_added", function(data) {           
          tamanho = data.val().nick;
        });
        if(tamanho.length == 0){
            dispatch({  type:VERIFICA_DISPONIBILIDADE,disponibilidade:true });
      }else{
        dispatch({  type:VERIFICA_DISPONIBILIDADE,disponibilidade:false });
      }
         
       

    }
}

export const modificarUsername = (username) =>{
    return dispatch => {
        var playersRef = firebase.database().ref('PROFILE_USER/');
        let tamanho="";
        playersRef.orderByChild("nick").equalTo(username).on("child_added", function(data) {           
          tamanho = data.val().nick;
        });
        if(tamanho.length == 0){
            dispatch({  type:VERIFICA_DISPONIBILIDADE,disponibilidade:true });
      }else{
        dispatch({  type:VERIFICA_DISPONIBILIDADE,disponibilidade:false });
      }
        dispatch({  type:USER_NAME_CADASTRO,payload:username });            
    }
}

export const modificaNome = (nome) =>{
    return {
        type:NOME_CADASTRO,
        payload:nome
    }
}

export const modificarEmail = (email) => {
    return {
        type:MODIFICAR_EMAIL,
        payload:email
    }
}

export const modificarSenha = (senha) => {
    return {
        type:MODIFICAR_SENHA,
        payload: senha
    }
}

export const modificaPin = (pin) =>{
    return {
        type:MODIFICAR_PIN,
        payload: pin
    }
}


/*----------------------------------------------- FIM CADASTRO -------------------------------------*/

const generateKeyB64 = (val) =>{
    return b64.encode(val);
}
const generateHashID = () =>{
    return Math.floor((Math.random() * 6000) * 2018 * 2017);
}
