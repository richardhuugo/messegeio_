/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { Provider } from 'react-redux';
import Routes from './Routes';
import configure from './store/store';
import { PersistGate } from 'redux-persist/es/integration/react'
import firebase  from 'firebase';
import {ActivityIndicator,AsyncStorage} from 'react-native';
import './ReactotronConfig'
const { persistor, store } = configure()

export default class App extends React.Component {
 
  componentWillMount(){


  console.disableYellowBox = true;
  var config = {
    apiKey: "AIzaSyAKa7QD0R_4gdW14dJvRVVzONADXFHg0dw",
    authDomain: "messegeio-4a579.firebaseapp.com",
    databaseURL: "https://messegeio-4a579.firebaseio.com",
    projectId: "messegeio-4a579",
    storageBucket: "",
    messagingSenderId: "748250631491"
  };
  if (!firebase.apps.length) {
    firebase.initializeApp(config);}
 
   
}

   
  render() {
    const onBeforeLift = () => {
      // take some action before the gate lifts


    }
    
    return (
      <Provider store={store}>
       <PersistGate 
          loading={<ActivityIndicator />}
          onBeforeLift={onBeforeLift}
          persistor={persistor}>
          <Routes />
        </PersistGate>
      </Provider>
    );
  }
}

