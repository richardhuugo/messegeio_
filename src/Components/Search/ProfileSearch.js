import React,{Component} from 'react';
import {
    Image,
    Title,
    Text,
    Tile,
    Caption,
    Button,
    View,
    Html,
    Screen,
    ImageBackground,
    NavigationBar,
    Card, Row,
    Subtitle,Divider,
    InlineGallery,
    ScrollView,
    ImageGallery,
    DropDownMenu,
    ImagePreview,Overlay,
    ImageGalleryOverlay
    } from '@shoutem/ui';

  import {Dimensions, Linking,BackHandler,TouchableHighlight} from 'react-native';
  
  import { StyleProvider } from '@shoutem/theme';
  import { Actions } from 'react-native-router-flux';
  import Icone from 'react-native-vector-icons/MaterialCommunityIcons';

  import {visualizarUserContagem, AdicionarUsuario,verificaContato} from '../../Actions/HomeActions';
  import {connect} from 'react-redux';

class ProfileSearch extends React.Component {

  componentDidMount() {
    this.props.verificaContato(this.props.email, this.props.myemail)
    this.props.visualizarUserContagem(this.props.email,this.props.myemail);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }
    constructor(props){
        super(props);
        console.ignoredYellowBox = [
          'Setting a timer'
          ];




      }
      _adicionarContato(){

        this.props.AdicionarUsuario({id:this.props.idProfile, myemail:this.props.myemail, email:this.props.email,foto:this.props.pic, nome: this.props.nome})
      }
      _verificaContato(){
          if(this.props.usuario_final){
            return(
              <View>
              </View>
            );
          }else{
            if(this.props.meuContato){
              return(
                <TouchableHighlight onPress={add =>{false}}>
                  <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                  <Icone name="account-check" size={15} color="white" />
                  </Overlay>
                 </TouchableHighlight>
              );
            }
            return(
              <TouchableHighlight onPress={add =>{this._adicionarContato()}}>
                <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                <Icone name="account-plus-outline" size={15} color="white" />
                </Overlay>
               </TouchableHighlight>
            );

          }
          
        
      }

    render(){
        return(
            <View >




                <ScrollView>
                <View>
                <Screen>
                <ImageBackground
       animationName="hero"
            styleName="large-square placeholder"
            source={ require('../../img/cali.jpg') }
      >
      <Tile animationName="hero">
                <Image   styleName="medium-avatar" source={{uri:this.props.pic}}/>
              <Subtitle>{this.props.nome}</Subtitle>
            <View style={{ flexDirection:'row'}}>

              <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                <Subtitle styleName="top">Map</Subtitle>
              </Overlay>
              <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                <Subtitle styleName="top">Map</Subtitle>
              </Overlay>
              {this._verificaContato()}



              </View>

            </Tile>


      </ImageBackground>
      <View

      >
        <Tile>

          <View styleName="content">
            <Title>MAUI BY AIR THE BEST WAY AROUND THE ISLAND</Title>
            <View styleName="horizontal space-between">
              <Caption>{this.props.estado}</Caption>
              <Caption>15:34</Caption>
            </View>
          </View>
        </Tile>
      </View>
                </Screen>




        </View>





        </ScrollView>

          </View>

        );
    }
}
const mapStateToProps = (state) => ({
  usuario_final: state.HomeReducer.usuario_final,
  estado: state.HomeReducer.estado,
  meuContato: state.HomeReducer.meuContato,
  myemail: state.AuthReducer.emaill
})

export default connect(mapStateToProps,{visualizarUserContagem,AdicionarUsuario,verificaContato})(ProfileSearch)
