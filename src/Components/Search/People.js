import React from 'react';
import { Text,Image,StyleSheet, View } from 'react-native';
import { TouchableOpacity } from '@shoutem/ui';
import { Actions } from 'react-native-router-flux';
export default class People extends React.Component {


	_render(){
			let dados =	this.props.dados
			if(this.props.dados == ""){
				return(<Text></Text>)									
			}else{

				return(
					<View style={stilo.ViewImagem}>
							<TouchableOpacity 
							onPress={click => {
								Actions.ProfileSearch({
									idProfile:dados.idProfile, 
									email:dados.email, 
									nome:dados.nome, 
									pic:dados.pic
									})}}>
									<Image style={stilo.Imagem} source={{ uri: dados.pic}}/>
									<View style={stilo.texto}>
										<Text>{dados.nick}</Text>
										<Text>{dados.nome}</Text>
										<Text>{dados.idProfile}</Text>
										<Text>{dados.email}</Text>
									</View>
							</TouchableOpacity>
					</View>	)
			}
		}

	render() {
		return (<View>{this._render()}</View>);
	}
}


const stilo = StyleSheet.create({
  Imagem: {
      width:80, height:80  	,
      backgroundColor: 'white'
  },
  texto:{
  	marginLeft: 10,
  	flex:1
  },
  ViewImagem:{
  flex: 1,
  flexDirection: 'row'
  },
});
