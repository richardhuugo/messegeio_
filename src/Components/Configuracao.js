import React from 'react';


import { Card, Button,Divider,Header } from 'react-native-elements'
import {
  Row,
  View,
  Text,
  Switch,
  Subtitle
    } from '@shoutem/ui';

export default class Configuracao extends React.Component {
  constructor() {
  super();
  this.state = {
    switchOn: true,
  };
}
_verificaStatus(){
  if(this.state.switchOn){
    return(
      <Text numberOfLines={1}>Ativo</Text>
    );
  }
  return (
    <Text numberOfLines={1}>Inativo</Text>
  );
}

  render() {
    const { switchOn } = this.state;
    return (

            <View style={{backgroundColor: 'white'}} >
                <Header

                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'Privacidade', style: { color: '#fff' } }}

                  />

                  <Row styleName="small" style={{justifyContent: 'space-between'}} >

                  <View styleName="vertical">
                    <Subtitle>Visualização do perfil</Subtitle>
                    {this._verificaStatus()}
                    </View>
                      <Switch
                               onValueChange={value => {this.setState({ switchOn: value});                               

                                }}
                               value={switchOn}
                             />
                  </Row>
                    <Divider style={{ backgroundColor: 'lightgray' }} />
            </View>
    );
  }
}
