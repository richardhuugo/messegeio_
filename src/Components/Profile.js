import React,{Component} from 'react';
import {
    Image,
    Title,
    Text,
    Tile,
    Caption,
    Button,
    Icon,
    View,
    Html,
    Screen,
    ImageBackground,
    NavigationBar,
    Card, Row,
    Subtitle,Divider,
    InlineGallery,
    ScrollView,
    ImageGallery,
    DropDownMenu,
    ImagePreview,Overlay,Switch,
    ImageGalleryOverlay
    } from '@shoutem/ui';

  import { Dimensions, Linking, BackHandler,ActivityIndicator, TouchableHighlight,StyleSheet } from 'react-native';
  import { StyleProvider } from '@shoutem/theme';
  import { Actions } from 'react-native-router-flux';
  import Icone from 'react-native-vector-icons/MaterialCommunityIcons';
  import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
  import {privacidateUser,changeViewProfile} from '../Actions/HomeActions';
  import {connect} from 'react-redux';

class Profile extends React.Component {

  componentDidMount() {
    this.props.privacidateUser(this.props.myemail);
    
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }
    constructor(props){
        super(props);
        console.ignoredYellowBox = [
          'Setting a timer'
          ];

          this.state = {
            switchOn: false,
          };

      }
      _changeViewProfile(val){
          this.props.changeViewProfile(val,this.props.myemail)
          
      }
     
     _renderView(){
       if(this.props.load_privacidade){
          
            return(<ActivityIndicator size="large" />);
          
       }
       return(
        <View>
            <View >
                          <Divider styleName="section-header">
                          <Caption>INFORMAÇÕES BÁSICAS</Caption>
                        </Divider>
                          <View   >

                          <Row styleName="small">
                              <Icon name="share" />
                              <View styleName="vertical">
                              <Subtitle>Bridges Rock Gym</Subtitle>
                              <Text numberOfLines={1}>www.example.com/deal/link/that-is-really-long</Text>
                              </View>
                              <Icon styleName="disclosure" name="right-arrow" />
                              </Row>
                          </View>
                </View>


                <View >
                        <Divider styleName="section-header">
                        <Caption>PRIVACIDADE</Caption>
                      </Divider>
                        <View   >

                          <Row styleName="small">
                              <Icon name="share" />
                              <View styleName="vertical">
                              <Subtitle>Visualização perfil</Subtitle>
                              <Text numberOfLines={1}>TEXTO APROPRIADO</Text>
                              </View>
                              <Switch
                                  onValueChange={value => {this._changeViewProfile(value)}}
                                  value={this.props.viewProfile}
                                />
                             
                              </Row>
                        </View>
              </View>

              <View >
                      <Divider styleName="section-header">
                      <Caption>NOTIFICAÇÕES</Caption>
                    </Divider>
                      <View  >

                      <Row styleName="small">
                          <Icon name="share" />
                          <View styleName="vertical">
                          <Subtitle>Bridges Rock Gym</Subtitle>
                          <Text numberOfLines={1}>www.example.com/deal/link/that-is-really-long</Text>
                          </View>
                          <Switch
                              onValueChange={value => {false}}
                              value={true}
                            />
                          </Row>
                      </View>
            </View>

        </View>
       );
     }

    render(){
      

        return(




    <View>

          <ScrollView>

                <View>
                        <Screen>
                                  <ImageBackground
                                  animationName="hero"
                                  styleName="large-wide"
                                  source={ require('../img/cali.jpg') }
                                  >




                                        <Tile animationName="hero">
                                              <Image   styleName="medium-avatar" source={{uri:'https://thumbs.dreamstime.com/b/%C3%ADcone-do-sinal-do-usu%C3%A1rio-s%C3%ADmbolo-da-pessoa-avatar-humano-84519083.jpg'}}/>
                                              <Subtitle>hugo richard</Subtitle>
                                                  <View style={{ flexDirection:'row'}}>
                                                        <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                                                          <Subtitle styleName="top">Map</Subtitle>
                                                        </Overlay>
                                                        <Overlay styleName="rounded-small image-overlay" style={{ margin:10}}>
                                                          <Subtitle styleName="top">Map</Subtitle>
                                                        </Overlay>
                                                        <Overlay  styleName="rounded-small image-overlay" style={{ margin:10}}>
                                                            <TouchableHighlight onPress={click => {Actions.Configuracao()} }>
                                                              <Icone name="account-key" size={15} color="white" />
                                                            </TouchableHighlight>
                                                        </Overlay>
                                                  </View>
                                          </Tile>
                                  </ImageBackground>
                        </Screen>

                  </View>
                  {this._renderView()}
              </ScrollView>

  </View>

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  estilo:{
    padding:20
  }
});

const mapStateToProps = state =>({
  viewProfile: state.HomeReducer.viewProfile,
  load_privacidade: state.HomeReducer.load_privacidade,
  viewProfile:state.HomeReducer.viewProfile,
  myemail:state.AuthReducer.email

});

export default connect(mapStateToProps,{privacidateUser,changeViewProfile})(Profile);