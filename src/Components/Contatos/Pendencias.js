import * as React from 'react';
import { View, StyleSheet, Text,ScrollView ,ListView,TouchableHighlight} from 'react-native';
import {GridRow,TouchableOpacity,Card ,Image,Subtitle, Caption } from '@shoutem/ui';

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import {fecthUserRecebidos,aceitarDeletarUser} from '../../Actions/HomeActions';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';

 class Pendencias extends React.Component{
  
  constructor(props){
    super(props);
    console.ignoredYellowBox = [
      'Setting a timer'
      ];
  }
  
  componentWillMount(){
    this.props.fecthUserRecebidos(this.props.myemail);
    this.criaFontDeDados(this.props.contatos);
  
  
  }

  componentWillReceiveProps(nextProps){
    this.criaFontDeDados(nextProps.contatos);
   
  }
  criaFontDeDados( contatos){
    const ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 != r2});
    this.fontDeDados = ds.cloneWithRows(contatos);
  }

  _adicionarDeletar(val, condicion){
    this.props.aceitarDeletarUser(val, condicion, this.props.myemail)
    
  }
  renderRow(contato){
    return(
     
      <View style={{padding:2}} >
     <TouchableHighlight
         underlayColor="transparent"
         onPress={r => {false}}

      >
     <View style={{ borderStyle:'solid' ,borderColor:'lightgray',padding:10,borderBottomWidth: 1, flexDirection:'row', justifyContent:'space-between'}} >
  
      <View style={{marginLeft:5, flexDirection:'row'}} >
   
     <Image
          style={{width:68,height:65}}
          styleName="medium-avatar"
          source={{ uri: contato.pic}}
        />
       
  <View style={{marginLeft:10}} >
          <Text >{contato.nome}</Text>
          <Text >{contato.nome}</Text>
          
        </View>
        </View>
       
        <View style={{marginLeft:10, justifyContent:'center'}} >

<Button
title="Aceitar"
fontSize={10}
onPressOut={
  () => this._adicionarDeletar(contato.emailSolicitante,true)
}
buttonStyle={{
  backgroundColor: "green",
  width: 70,
  height: 15,
  borderColor: "transparent",
  borderWidth: 0,
  borderRadius: 5
}}
containerStyle={{ marginTop: 20 }}
/>

<Button
onPressOut={
() => this._adicionarDeletar(contato.emailSolicitante,false)
}
title="Deletar"
fontSize={10}
buttonStyle={{
  backgroundColor: "red",
  width: 70,
  height: 15,
  marginTop:10,
  borderColor: "transparent",
  borderWidth: 0,
  borderRadius: 5
}}
containerStyle={{ marginTop: 20 }}
/>        
        </View>
        
      </View>
      </TouchableHighlight>
      </View>
     
    );
  }
  render(){
    return(

      <ListView 
      enableEmptySections
       dataSource={this.fontDeDados}
       renderRow={this.renderRow.bind(this)  }/>  
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });

  mapStateToProps = state => {
    const contatos = _.map(state.ListarReducerSolicitacao, (val, uid) =>{
      return { ...val, uid}
    })
    return { contatos}
  }
export default connect(mapStateToProps,{fecthUserRecebidos,aceitarDeletarUser})(Pendencias);