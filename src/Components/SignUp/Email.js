import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar,TextInput,BackHandler} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {modificarEmail} from '../../Actions/AuthActions';



class Email extends React.Component{

  constructor(props){
      super(props);

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }
    _verificaEmail(email){

        Actions.PassPage({email, nome:this.props.nome, user_name:this.props.user_name});
    }
    render(){


        return(
            <View style={{flex:1,  backgroundColor:'#1abc9c', padding:20}}>
<StatusBar
            backgroundColor="#1abc9c"
            barStyle="light-content"
          />
                <View style={{flex:1, alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                <TouchableOpacity 
                          style={{backgroundColor:'transparent',


                                  alignItems:'center',
                                
                                  
                                  padding:10
                                  }}
                                  onPress={login =>{Actions.pop()}}
                          >
                              <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                          </TouchableOpacity>
                </View>


                <View style={{flex:2}}>
              <Text
                style={{fontSize:20,
                  color:'white',
                  marginBottom:20
                }}
              >Qual é o seu email ?</Text>                
              <TextInput
                style={{fontSize:18, height:45}}
                        value={this.props.modificarEmail}
                        onChangeText={email =>{this.props.modificarEmail(email)}}
                        placeholder="Email"                         
                        placeholderTextColor='#fff' 
              />
            <View style={{alignItems:'flex-end',marginTop:30}} >
              <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                  borderRadius:70,
                                  borderColor: 'white',
                                  borderWidth: 1,
                                  marginTop:10,
                                  width:60,
                                  }}
                                  onPress={login =>{this._verificaEmail(this.props.email)}}
                          >
                              <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:38}}> ></Text>
                          </TouchableOpacity>
                          </View>
                </View>
               

            </View>


        );
    }
}


const  mapStateToProps =state=> ({
    email : state.AuthReducer.email
})
export default connect(mapStateToProps, {modificarEmail})(Email);

