import React from 'react';
import {View,
     ScrollView,
     Text,
     TouchableHighlight,
     ActivityIndicator,
     StatusBar,
     TextInput,
    BackHandler,
    TouchableOpacity
  } from 'react-native';

import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

import {modificarUsername,verificaUsername} from '../../Actions/AuthActions';



 class Username extends React.Component{

  constructor(props){
      super(props);

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
       Actions.pop();
        return true;
    }


    _verificaUsername(username){
                 this.props.verificaUsername(username)
               
                 if(this.props.disponibilidade){
                 Actions.EmailPage({user_name:username, nome:this.props.nome});
                 }else{

                 }
         
      
    }
   _verificaDisponibilidade(){    
       if(this.props.disponibilidade){
            return(
                <TouchableOpacity 
                style={{backgroundColor:'transparent',

                
                        alignItems:'center',
                        borderRadius:70,
                        borderColor: 'white',
                        borderWidth: 1,
                        marginTop:10,
                        width:60,
                        }}
                        onPress={login =>{this._verificaUsername(this.props.user_name)}}
                >
                    <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:38}}> ></Text>
                </TouchableOpacity>
            );
       }
       return(
      <View></View>
    );

   }

   _exibeText(){
       
       if(this.props.disponibilidade){
        return(
            <View>
                <Text>Disponivel</Text>
            </View>
        );
       }
       if(this.props.disponibilidade == false){
           return(
               <View>
                   <Text>Indisponivel</Text>
               </View>
           );
       }

       return(
        <View>           
        </View>
    );
   }
    render(){


        return(
            <View style={{flex:1,  backgroundColor:'#1abc9c', padding:20}}>
<StatusBar
            backgroundColor="#1abc9c"
            barStyle="light-content"
          />
                <View style={{flex:1, alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                
                                  
                                  padding:10
                                  }}
                                  onPress={login =>{Actions.pop()}}
                          >
                              <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                          </TouchableOpacity>
                </View>


                <View style={{flex:2}}>
              <Text
                style={{fontSize:20,
                  color:'white',
                  marginBottom:20
                }}
              >Digite seu username ?</Text>                
              <TextInput
                style={{fontSize:18, height:45}}
                        value={this.props.user_name} 
                        onChangeText={username => {this.props.modificarUsername(username)}}
                        placeholder="UserName"                         
                        placeholderTextColor='#fff' 
              />
              {this._exibeText()}
            <View style={{alignItems:'flex-end',marginTop:30}} >
            {this._verificaDisponibilidade()}
                          </View>
                </View>
               

            </View>


        );
    }
}


const mapStateToProps = state =>({
    user_name: state.AuthReducer.user_name,
    disponibilidade: state.AuthReducer.disponibilidade
});
export default connect(mapStateToProps,{modificarUsername,verificaUsername})(Username);