import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar,TextInput,BackHandler} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {modificarSenha} from '../../Actions/AuthActions';



class Password extends React.Component{

  constructor(props){
      super(props);

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }

   _verificaSenha(senha){
    Actions.PassKeyPage({senha,nome:this.props.nome,email:this.props.email, user_name:this.props.user_name});
   }
    render(){


        return(
            <View style={{flex:1,  backgroundColor:'#1abc9c', padding:20}}>
<StatusBar
            backgroundColor="#1abc9c"
            barStyle="light-content"
          />
                <View style={{flex:1, alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                
                                  
                                  padding:10
                                  }}
                                  onPress={login =>{Actions.pop()}}
                          >
                              <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                          </TouchableOpacity>
                </View>


                <View style={{flex:2}}>
              <Text
                style={{fontSize:27,
                  color:'white',
                  marginBottom:20
                }}
              >Digite sua senha</Text>   
              <Text
              style={{fontSize:12,
                color:'white'
                
              }}
              >Sua senha deve conter pelo menos 6 caracteres</Text>             
              <TextInput
                style={{fontSize:18,marginTop:10, height:45}}
                      value={this.props.senha}
                        secureTextEntry
                        placeholder="Senha" 
                        onChangeText={senha =>{this.props.modificarSenha(senha)}}
                        placeholderTextColor='#fff' 
              />
            <View style={{alignItems:'flex-end',marginTop:30}} >
            
              <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                  borderRadius:70,
                                  borderColor: 'white',
                                  borderWidth: 1,
                                  marginTop:10,
                                  width:60,
                                  }}
                                  onPress={login =>{this._verificaSenha(this.props.senha)}}
                          >
                              <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:38}}> ></Text>
                          </TouchableOpacity>
                          </View>
                </View>
               

            </View>


        );
    }
}

const mapStateToProps = (state, ownProps) => ({
  senha: state.AuthReducer.senha
})


export default connect(mapStateToProps,{modificarSenha})(Password);