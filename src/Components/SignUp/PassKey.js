import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar,BackHandler,TextInput} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {cadastroUser,modificaPin} from '../../Actions/AuthActions';



class PassKey extends React.Component{

  constructor(props){
      super(props);

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }

  _verificarChavePin(pin){
  
    this.props.cadastroUser({
      nome_cadastro:this.props.nome, 
      email:this.props.email, 
      senha:this.props.senha,       
      pin_cadastro:pin,
      user_name:this.props.user_name});
    
  }
   
  _loadLogin(){

    if(this.props.load_cadastro){
        return(<ActivityIndicator size="large" />);
    }

  return(      <TouchableOpacity 
    style={{backgroundColor:'transparent',

    
            alignItems:'center',
            borderRadius:10,
            borderColor: 'white',
            borderWidth: 1,
            marginTop:10,
            padding:10
            }}
            onPress={login =>{this._verificarChavePin(this.props.pin_cadastro)}}
    >
        <Text style={{color:'white',  fontSize:16}}> Finalizar</Text>
    </TouchableOpacity>);
}
    render(){
      

        return(
            <View style={{flex:1,  backgroundColor:'#1abc9c', padding:20}}>
<StatusBar
            backgroundColor="#1abc9c"
            barStyle="light-content"
          />
                <View style={{flex:1, alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                
                                  
                                  padding:10
                                  }}
                                  onPress={login =>{Actions.pop()}}
                          >
                              <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                          </TouchableOpacity>
                </View>


                <View style={{flex:2}}>
              <Text
                style={{fontSize:27,
                  color:'white',
                  marginBottom:20
                }}
              >Digite um pin</Text>   
              <Text
              style={{fontSize:12,
                color:'white'
                
              }}
              >Defina um pin de no minimo 6 caracteres para nós criptografarmos suas mensagens. </Text>             
              <TextInput
                style={{fontSize:18,marginTop:10, height:45}}
                        value={this.props.pin_cadastro}
                        secureTextEntry
                        placeholder="Pin" 
                        onChangeText={val =>{this.props.modificaPin(val)}}
                        placeholderTextColor='#fff' 
              />
            <View style={{alignItems:'center',marginTop:30}} >
            
            {this._loadLogin()}

                          </View>
                </View>
               

            </View>


        );
    }
}
const mapStateToProps = state => ({
  pin_cadastro: state.AuthReducer.pin_cadastro,
  load_cadastro: state.AuthReducer.load_cadastro
})

export default connect(mapStateToProps,{modificaPin,cadastroUser})(PassKey);
