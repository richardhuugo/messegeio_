import React from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  TouchableHighlight
} from 'react-native';
import {connect} from 'react-redux';
import {loggout} from '../Actions/HomeActions';



const window = Dimensions.get('window');
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: 'white',
    padding: 20,
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20,
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1,
  },
  name: {
    position: 'absolute',
    left: 70,
    top: 20,
  },
  item: {
    fontSize: 14,
    fontWeight: '300',
    paddingTop: 5,
  },
});

 class Menu extends React.Component {
  
    render(){
      return (
        <ScrollView scrollsToTop={false} style={styles.menu}>
          <View style={styles.avatarContainer}>
            <Image
              style={styles.avatar}
              source={{ uri }}
            />
            <Text style={styles.name}>Your name</Text>
          </View>
    
         
             <TouchableHighlight onPress={logout =>{ this.props.loggout(this.props.email_login)}} >
                        <Text  style={styles.item} > Sair</Text>
                    </TouchableHighlight>
          
    
          <Text
            onPress={() => this.props.onItemSelected('Contacts')}
            style={styles.item}
          >
            Contacts
          </Text>
        </ScrollView>
      );
   }
 
}

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired,
};
const mapStateToProps = state => ( {
  email_login: state.AuthReducer.email_login
})

export default connect(mapStateToProps,{loggout})(Menu);
