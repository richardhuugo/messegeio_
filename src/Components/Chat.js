import React from 'react'

import {connect} from 'react-redux'
import {chaveContatos,mensagemContato,mensagemEnviar} from '../Actions/HomeActions'
import b64 from 'base-64'
import {View,Text, StyleSheet,TouchableHighlight,TextInput} from 'react-native'
import Icone from 'react-native-vector-icons/MaterialCommunityIcons';
import {Image,Spinner,Caption,Icon,DropDownMenu,Title, TouchableOpacity,NavigationBar} from '@shoutem/ui';
import {Header,Avatar} from 'react-native-elements'

import { Actions } from 'react-native-router-flux';

 class Chat extends React.Component{
    constructor(props){
        super(props)
     
    }
    handleBackButton() {   
        Actions.pop();   
          return true;
      }
  
    _enviarMessage() {
     
  
        if(this.props.mensagemSend.length != 0){
            this.props.chaveContatos({myPrivateKey:this.props.myKey,publickeyUser:this.props.chave})
            this.props.mensagemContato({
                myEmail:this.props.myEmail,
                message:this.props.mensagemSend, 
                touser:this.props.emailUser,
                keyMessage:this.props.keyMessage,
                idKey:this.props.idKey
            })
        }else{
          alert('vazio')
        } 
      
    }      
    
        
    
    //mensagemEnviar

    render(){
        return(
            <View style={{flex:1, backgroundColor:'#fff'}} >
            <View style={{flex:1, backgroundColor:'green', padding:5, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <TouchableOpacity
            onPress={() => {this.handleBackButton()}}
            >
                <Icone  name="chevron-left" size={30} color="gray" />
            </TouchableOpacity>
            
            <Text>{this.props.nome}</Text>
            <Avatar
                    
                    medium
                    rounded
                    source={{uri: this.props.pic}}
                    onPress={() => {alert('click')}}
                    activeOpacity={0.7}
                />
            </View>
            
               <View style={{flex:5, backgroundColor:'blue'}}  >  
               <Text>painel</Text>
               </View>



               
               <View style={{height:60, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                   <TextInput
                       value={this.props.mensagemSend}
                       onChangeText={ texto => {this.props.mensagemEnviar(texto) }}
                    style={{flex:4, borderRadius:10, backgroundColor:'#fff', fontSize:18}}/>

                   <TouchableOpacity style={{backgroundColor:'gray', padding:10, borderRadius:50}} onPress={ () => {this._enviarMessage()} } underlayColor="#fff" >
                      <Text
                      style={{color:'white'}}
                      >send</Text>
                   </TouchableOpacity>
               </View>
           </View>
        )
    }
}
const mapStateToProps = state => ( {
    myKey: state.AuthReducer.privateKey,
    myEmail: state.AuthReducer.email_login,
    keyMessage: state.MessagesReducer.keyMessage,
    mensagemSend: state.MessagesReducer.mensagemSend
})
export default connect(mapStateToProps,{chaveContatos,mensagemContato,mensagemEnviar})(Chat)
