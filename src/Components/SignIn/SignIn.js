import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar,TextInput,BackHandler} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {modificarSenhaLogin, modificarEmailLogin} from '../../Actions/AuthActions';



class SignIn extends React.Component{

    constructor(props){
      super(props);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      Actions.pop();
        return true;
    }
   
    render(){


        return(
            <View style={{flex:1,  backgroundColor:'#3498db', padding:20}}>
                    <StatusBar
                        backgroundColor="#3498db"
                        barStyle="light-content"
                    />
                    <View style={{ alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                        <TouchableOpacity 
                            style={{backgroundColor:'transparent',
                            alignItems:'center',                    
                            padding:10
                            }}
                            onPress={login =>{Actions.pop()}}
                        >
                            <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <Text
                            style={{
                            fontSize:20,
                            color:'white',
                            marginBottom:20
                            }}
                        >
                        Login
                        </Text> 
                    </View>

                <View style={{flex:2}}>
                            
                        <TextInput
                            style={{fontSize:18, height:45}}
                                    value={this.props.email}
                                    onChangeText={email =>false}
                                    placeholder="Email"                         
                                    placeholderTextColor='#fff' 
                                    onChangeText={email =>{this.props.modificarEmailLogin(email)}}
                        />
                        <TextInput
                        style={{fontSize:18, height:45}}
                                secureTextEntry
                                value={this.props.senha}
                                onChangeText={email =>false}
                                placeholder="Senha"                         
                                placeholderTextColor='#fff' 
                                onChangeText={senha => {this.props.modificarSenhaLogin(senha)}}
                        />
                        <View style={{alignItems:'center',marginTop:30}} >
                                <TouchableOpacity 
                                        style={{backgroundColor:'transparent',                          
                                                alignItems:'center',
                                                borderRadius:70,
                                                borderColor: 'white',
                                                borderWidth: 1,
                                                marginTop:10,
                                                width:60,
                                                }}
                                    onPress={login =>{Actions.ChaveSecret({email: this.props.email, senha:this.props.senha})}}
                                >
                                <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:38}}> ></Text>
                                </TouchableOpacity>
                        </View>
                </View>
               <View style={{justifyContent:'center',alignItems:'center' }} >
                    <TouchableOpacity 
                        style={{
                            backgroundColor:'transparent',                        
                            alignItems:'center',                               
                            }}
                            onPress={login =>{false}}
                    >
                        <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:18}}> Esquecia a senha</Text>
                    </TouchableOpacity>
               </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    email: state.AuthReducer.email,
    senha: state.AuthReducer.senha
});

export default connect(mapStateToProps,{modificarSenhaLogin,modificarEmailLogin})(SignIn);