import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar,TextInput,BackHandler} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {modificarPin,loginUser} from '../../Actions/AuthActions';





 class ChaveSecret extends React.Component{

  constructor(props){
      super(props);

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    

    handleBackButton() {
      Actions.pop();
        return true;
    }
    _efetuarLogin(){
    
        this.props.loginUser({
            email:this.props.email, 
            senha:this.props.senha,
            pin: this.props.pin_cadastro})
    }
    render(){


        return(
            <View style={{flex:1,  backgroundColor:'#3498db', padding:20}}>
        <StatusBar
            backgroundColor="#3498db"
            barStyle="light-content"
          />
                <View style={{ alignItems:'flex-start',marginBottom:15,  justifyContent: 'flex-start'}}>
                <TouchableOpacity 
                          style={{backgroundColor:'transparent',


                                  alignItems:'center',
                                
                                  
                                  padding:10
                                  }}
                                  onPress={login =>{Actions.pop()}}
                          >
                              <Text style={{color:'white', marginBottom:5, fontSize:12}}> Back </Text>
                          </TouchableOpacity>
                </View>
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text
                style={{fontSize:20,
                  color:'white',
                  marginBottom:20
                }}
              >Pin</Text> 

               

                </View>

                <View style={{flex:2}}>
                            
              <TextInput
                style={{fontSize:18, height:45}}
                        value={this.props.pin_cadastro}
                        onChangeText={pin =>{this.props.modificarPin(pin)}}
                        placeholder="Pin"                         
                        placeholderTextColor='#fff' 
                        secureTextEntry
              />
         
            <View style={{alignItems:'center',marginTop:30}} >
              <TouchableOpacity 
                          style={{backgroundColor:'transparent',

                          
                                  alignItems:'center',
                                  borderRadius:70,
                                  borderColor: 'white',
                                  borderWidth: 1,
                                  marginTop:10,
                                  width:60,
                                  }}
                                  onPress={login =>{this._efetuarLogin()}}
                          >
                              <Text style={{color:'white', marginBottom:5,marginRight:5, fontSize:38}}> ></Text>
                          </TouchableOpacity>
                          </View>
                </View>
            

            </View>


        );
    }
}


const mapStateToProps = state => ({
    pin_cadastro: state.AuthReducer.pin_cadastro
    
});

export default connect(mapStateToProps,{modificarPin,loginUser})(ChaveSecret);