import * as React from 'react';
import { View, StyleSheet, Text,ScrollView ,ListView,TouchableHighlight} from 'react-native';
import {GridRow,TouchableOpacity,Card ,Image,Subtitle, Caption } from '@shoutem/ui';
import b64 from 'base-64'
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {fetchMyContacts,chaveContatos} from '../Actions/HomeActions';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';

 class Contatos extends React.Component{
  
  constructor(props){
    super(props);
    console.ignoredYellowBox = [
      'Setting a timer'
      ];

  

  }


  componentWillMount(){
    this.props.fetchMyContacts(this.props.myEmail);
    this.criaFontDeDados(this.props.contatos);
  
  
  }

  componentWillReceiveProps(nextProps){
    this.criaFontDeDados(nextProps.contatos);
   
  }
  criaFontDeDados( contatos){
    const ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 != r2});
    this.fontDeDados = ds.cloneWithRows(contatos);
  }




  renderRow(contato){
    
   //this.props.chaveContatos({key:contato.keysMessage, idKey:contato.idKey, email:contato.emailUser})
  
    return(
        <TouchableOpacity
        onPress={ver =>{
       
          Actions.Chat({
          chave:contato.keysMessage,
          idKey:contato.idKey, 
          emailUser:contato.emailUser ,
          pic:contato.pic,
          nome: contato.name
        })
      }}
      >
      <View style={{ borderStyle:'solid' ,borderColor:'lightgray',padding:10,borderBottomWidth: 1, flexDirection:'row', justifyContent:'flex-start'}} >
      <TouchableOpacity
        onPress={ver =>{Actions.ProfileSearch({pic:contato.pic})}}
      >
      <View style={{marginLeft:5}} >
      <Image
            style={{width:68,height:65}}
            styleName="medium-avatar"
            source={{ uri: contato.pic}}
          />
        </View>
        </TouchableOpacity>
        <View style={{marginLeft:10}} >
          <Text >{contato.name}</Text>
          <Caption>{contato.status}</Caption>
        </View>
      
      </View>
      </TouchableOpacity>
     
    );
  }
  render(){

    return(
    
      <ListView 
      enableEmptySections
       dataSource={this.fontDeDados}
       renderRow={this.renderRow.bind(this)  }/> 
      
    );
  }
}


  mapStateToProps = state => {
    const contatos = _.map(state.ListarReducerContatos, (val, uid) =>{
      return { ...val, uid}
    })
    return { contatos}
  }
export default connect(mapStateToProps,{fetchMyContacts,chaveContatos})(Contatos);