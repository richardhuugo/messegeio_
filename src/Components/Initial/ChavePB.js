import React from 'react';
import {View, Text, InputText, Button, StyleSheet} from 'react-native';
import Carousel  from 'react-native-carousel';
import Mensagem1 from './msg/Mensagem1';
import Mensagem2 from './msg/Mensagem2';
import Mensagem3 from './msg/Mensagem3';
import Mensagem4 from './msg/Mensagem4';
import Cad from '../Initial/Cadastro';
import { Actions } from 'react-native-router-flux';
export default class ChavePB extends React.Component{

    render(){
        return(

               <Carousel  animate={false} loop={false} indicatorSize={20} indicatorOffset={0} hideIndicators={true}>

                <Mensagem1 />
                <Mensagem2 />
                <Mensagem3 />
                <Mensagem4 nome_auth={this.props.nome_user} idhash={this.props.idhash} />
              </Carousel>

        );
    }
}
var styles = StyleSheet.create({
    container: {
      width: 375,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    },
  });
