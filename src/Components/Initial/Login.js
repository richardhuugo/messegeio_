import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    TextInput,

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import  { loginUser , modificarSenhaLogin, modificarEmailLogin} from '../../Actions/AuthActions';



class Login extends React.Component{

  constructor(props){
      super(props);

  }
    getRestaurant() {
        return {
          name: "Gaspar Brasserie",
          address: "185 Sutter St, San Francisco, CA 94109",
          url: "gasparbrasserie.com",
          image: { "url": "https://shoutem.github.io/restaurants/restaurant-1.jpg"},
          mail: "info@gasparbrasserie.com"
        };
      }


      _efetuarLogin(){
            this.props.loginUser({email_login:this.props.email_login, senha_login: this.props.senha_login})
      }
      _loadLogin(){

            if(this.props.load_cadastro){
                return(<ActivityIndicator size="large" />);
            }

          return(  <TouchableOpacity
            style={{backgroundColor:'#4527A0',
                    alignItems:'center',
                    borderRadius:10,
                    marginTop:5,
                    padding:10}}
                    onPress={login => {this._efetuarLogin()}}
            >
                <Text style={{color:'white'}}>Login</Text>
            </TouchableOpacity>);
      }
    render(){

        const driver = new ScrollDriver();


        const restaurant = this.getRestaurant();

        return(
            <View style={{flex:1,  backgroundColor:'white', padding:20}}>

                <View style={{flex:1, alignItems:'center',  justifyContent: 'center'}}>
                    <Text>Login</Text>
                </View>


                <View style={{flex:2}}>
                        <TextInput
                        placeholder={'email'}
                        style={{marginTop:5,
                            marginBottom:15,
                            backgroundColor:'#DCDCDC',
                             borderRadius:10}}
                             onChangeText={ email => { this.props.modificarEmailLogin(email)}}
                        />
                        <TextInput
                        style={{marginTop:5, marginBottom:15,backgroundColor:'#DCDCDC', borderRadius:10}}
                        placeholder={'Password'}
                        onChangeText={ senha => { this.props.modificarSenhaLogin(senha)}}
                        secureTextEntry />

                      {this._loadLogin()}

                        <View style={{flexDirection:'row',
                                    justifyContent: 'space-between'}}>
                            <TouchableHighlight onPress={() => {Actions.Cadastro();}}
                            underlayColor="transparent" >
                                <Text style={{marginLeft:10, marginTop:20}} >Criar Conta</Text>
                            </TouchableHighlight>

                            <TouchableHighlight onPress={() => Actions.NovaSenha()}
                            underlayColor="transparent" >
                            <Text style={{marginRight:10, marginTop:20}} >Esqueci a senha</Text>
                            </TouchableHighlight>
                        </View>

                </View>


            </View>


        );
    }
}
const mapStateToProps = state => ({
    email_login: state.AuthReducer.email_login,
    senha_login: state.AuthReducer.senha_login
});


export default connect(mapStateToProps,{ loginUser, modificarEmailLogin, modificarSenhaLogin })(Login);
