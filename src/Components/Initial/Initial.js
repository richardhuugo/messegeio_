import React from 'react';
import {View, ScrollView,Text,TouchableHighlight,ActivityIndicator,StatusBar, BackHandler,
    ToastAndroid} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,TimingDriver,
    FadeIn,
    ZoomIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    TextInput,

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';




export default class Login extends React.Component{

  constructor(props){
      super(props);

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }



      _efetuarLogin(){
            this.props.loginUser({email_login:this.props.email_login, senha_login: this.props.senha_login})
      }

      
    render(){


        return(
            
            <View style={{flex:1,  backgroundColor:'#3498db', padding:20}}>
                 <StatusBar
            barStyle="light-content"
            backgroundColor="#3498db"
            
          /> 
                <View style={{flex:1, alignItems:'center', marginLeft: 10,marginBottom:15,  justifyContent: 'center'}}>
                    <Text style={{fontSize:28, color:'white'}} >MessegeIO</Text>
                </View>


                <View style={{flex:1}}>
                
                <TouchableOpacity
                  style={{backgroundColor:'white',
                          alignItems:'center',
                          borderRadius:10,
                          marginTop:5,
                          padding:10}}
                          onPress={login =>{Actions.SignIn()}}
                  >
                      <Text style={{color:'#3498db'}}>Login</Text>
                  </TouchableOpacity>


                        <TouchableOpacity
                          style={{backgroundColor:'transparent',
                                  alignItems:'center',
                                  
                                  borderRadius:10,
                                  borderColor: 'white',
                                  borderWidth: 1,
                                  marginTop:5,
                                  padding:10}}
                                  onPress={login =>{Actions.Nome()}}
                          >
                              <Text style={{color:'white'}}>Cadastre - se</Text>
                          </TouchableOpacity>





                </View>


            </View>


        );
    }
}


