import React from 'react';
import {View, ScrollView,Text,TouchableHighlight} from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,
    FadeIn,
    ScrollDriver,
  } from '@shoutem/animation';
  
  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,    
    Subtitle,
    TextInput,
    
  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';


export default class NovaSenha extends React.Component{

    getRestaurant() {
        return {
          name: "Gaspar Brasserie",
          address: "185 Sutter St, San Francisco, CA 94109",
          url: "gasparbrasserie.com",
          image: { "url": "https://shoutem.github.io/restaurants/restaurant-1.jpg"},
          mail: "info@gasparbrasserie.com"
        };
      }

      

    render(){
        const restaurant = this.getRestaurant();
        const driver = new ScrollDriver();
        return(
            <View style={{flex:1, padding:5, backgroundColor:'white'}}>
              
                <View style={{flex:1, alignItems:'center',  justifyContent: 'center'}}>
                    <Text style={{fontSize:18, marginTop:20, color:'gray'}} >Recuperar Senha</Text>
                </View>
                
                
                <View style={{flex:1, padding:20}}>
                <TextInput 
                placeholder={'Username or email'}
                style={{marginTop:5, marginBottom:15,backgroundColor:'#DCDCDC', borderRadius:10}}
                />
                <TouchableOpacity style={{backgroundColor:'#4527A0', alignItems:'center', borderRadius:10, marginTop:5, padding:10}}>
                    <Text style={{color:'white'}}>Recuperar</Text>
                </TouchableOpacity>
                
             
                </View>
                <View style={{ padding:20, marginTop:35}} >
                        
                <View style={{flexDirection:'row',  justifyContent: 'center'}}>
                    <TouchableHighlight onPress={() => Actions.Login()} underlayColor="transparent" >
                    <Text style={{marginTop:35}} >Login</Text>
                    </TouchableHighlight>
                                       
                </View>
                </View>
                
            </View>

        );
    }
}