import React from 'react';
import {View, ScrollView,Text,TouchableHighlight, ActivityIndicator } from 'react-native';
import {
    Parallax,
    HeroHeader,
    FadeOut,
    FadeIn,
    ScrollDriver,
  } from '@shoutem/animation';

  import {
    ImageBackground,
    TouchableOpacity,
    Tile,
    Title,
    Subtitle,
    TextInput,

  } from '@shoutem/ui';
import {Actions} from 'react-native-router-flux';
import { cadastroUser, modificarEmail, modificarSenha, cadastroUserGoogle,modificaNome }  from '../../Actions/AuthActions';
import {connect } from 'react-redux';


class Cadastro extends React.Component{
    constructor(props){
        super(props);

    }
    componentDidUpdate(){

    }
    _cadastrarUsuario(){
        const {email_cadastro, senha_cadastro,nome_cadastro} = this.props;
        this.props.cadastroUser({nome_cadastro,email_cadastro, senha_cadastro});
    }
    _animarCadastro(){
        if(this.props.load_cadastro){
            return(<ActivityIndicator size="large" />);
        }
        return(
    <View>
    <TouchableOpacity onPress={() => { this._cadastrarUsuario() } }  style={{backgroundColor:'#4527A0', alignItems:'center', borderRadius:10, marginTop:5, padding:10}}>
        <Text style={{color:'white'}}>Avançar</Text>
    </TouchableOpacity>

    </View>
);
    }
    render(){


        return(
            <View style={{flex:1, padding:20, backgroundColor:'white'}}>
            <ScrollView>
                <View style={{flex:1, alignItems:'center',  justifyContent: 'center'}}>
                    <Text style={{fontSize:18, marginTop:20, color:'gray'}} >Criar Conta</Text>
                </View>


                <View style={{flex:2, padding:20, marginBottom:20}}>
                <TextInput
                placeholder={'Nome'}
                onChangeText={ nome => { this.props.modificaNome(nome) }}
                style={{marginTop:5, marginBottom:15,backgroundColor:'#DCDCDC', borderRadius:10}}
                />

                <TextInput
                placeholder={'E-mail'}
                onChangeText={ email => { this.props.modificarEmail(email)}}
                style={{marginTop:5, marginBottom:15,backgroundColor:'#DCDCDC', borderRadius:10}}
                />
                <TextInput
                onChangeText={ senha => { this.props.modificarSenha(senha)}}
                  style={{marginTop:5, marginBottom:15,backgroundColor:'#DCDCDC', borderRadius:10}}
                placeholder={'Password'} secureTextEntry />


          


                   <Text>{this.props.cadastro_mensagem}</Text>
                    {this._animarCadastro()}



                </View>

                <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                    <TouchableHighlight onPress={() => false} underlayColor="transparent" >
                    <Text style={{marginTop:35}} >Termos e condições</Text>
                    </TouchableHighlight>

                    <TouchableHighlight onPress={() => {   Actions.Login();  }} underlayColor="transparent">
                    <Text style={{ marginTop:35}} >Login</Text>
                    </TouchableHighlight>
                </View>


                </ScrollView>
            </View>

        );
    }
}

const mapStateToProps = state => ({
    email_cadastro: state.AuthReducer.email_cadastro,
    senha_cadastro:  state.AuthReducer.senha_cadastro,
    load_cadastro: state.AuthReducer.load_cadastro,
    cadastro_mensagem: state.AuthReducer.cadastro_mensagem,
    nome_cadastro: state.AuthReducer.nome_cadastro

});
export default connect(mapStateToProps, {cadastroUser, modificarSenha, modificaNome,modificarEmail,cadastroUserGoogle })(Cadastro);
