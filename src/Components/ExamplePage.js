import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  //FlatList,
  TouchableHighlight,
  ScrollView,
} from 'react-native'
import Conversas from './Conversas';
import Contatos from './Contatos';
import { Actions } from 'react-native-router-flux';
import IconBadge from 'react-native-icon-badge';
import Icone from 'react-native-vector-icons/FontAwesome';


class ExamplePage extends PureComponent {

  setNativeProps(props) {
    
    this.list.setNativeProps({
      scrollEnabled: props.shouldBeScrollable,
    })
  }

  constructor(props){
    super(props);
    
  }
 
  _renderRow = (index) => {
    
      if(index == 1){
 

        return (
        
        
             <View style={styles.rowContainer}>
            <Contatos myEmail={this.props.myEmail} />
             </View>
           
           
           
       
           
       
       )
      }
      return (
        <View>
           <View style={styles.rowContainer}>
          <Conversas />
           </View>
         
         </View>
       
     )
  }
  _cabecalhoDados(index){
    if(index == 1){
      return(
        <View style={{justifyContent:'space-between', flexDirection:'row'}} >
        <Text style={styles.text}>{this.props.title}</Text>
       
       
        <TouchableHighlight style={{justifyContent:'center'}} onPress={() => {false}} >
        <Icone name="user-o" size={20} color="#bf1313" />
        </TouchableHighlight>
        
        <TouchableHighlight
          onPress={Pendencias => {Actions.Pendencias({myemail:this.props.myEmail})}}
        >        
        <IconBadge
                MainElement={
                  <View style={{backgroundColor:'transparent',
                    width:40,
                    height:40,
                    margin:6,
                    justifyContent:'center',
                    alignItems:'center'
                  }}>
                  <Icone name="user-o" size={20} color="#bf1313" />
                  </View>
                }
                BadgeElement={
                  <Text style={{color:'#FFFFFF'}}>6</Text>
                }
                IconBadgeStyle={
                  {width:20,
                  height:20,
                  backgroundColor: 'lightblue'}
                }
                Hidden={false}
        />
        </TouchableHighlight>

        <TouchableHighlight 
        style={{justifyContent:'center', marginRight:20}} 
        onPress={() => {Actions.Search()}} >
        
        <Icone name="user-plus" size={20} color="gray" />
        </TouchableHighlight>
        </View>
      );
    }

    return(
      <View style={{justifyContent:'space-between', flexDirection:'row'}} >
      <Text style={styles.text}>{this.props.title}</Text>
      
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
     
        
        <ScrollView
          ref={node => {
            this.list = node;
            
          }}
        >
          
            {this._renderRow(this.props.index)}
          
        </ScrollView>
        {this._cabecalhoDados(this.props.index)}
        <View style={styles.seperator}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  seperator: {
    height: 1,
    backgroundColor: '#ccc',
  },
  text: {
    padding: 10,
    
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 20,
    borderRadius: 50,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
})



export default ExamplePage

const IMAGES = [
  'https://images.pexels.com/photos/8923/pexels-photo.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/54632/cat-animal-eyes-grey-54632.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/4602/jumping-cute-playing-animals.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/137049/pexels-photo-137049.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/66292/cat-eyes-view-face-66292.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/8923/pexels-photo.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/54632/cat-animal-eyes-grey-54632.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/4602/jumping-cute-playing-animals.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/137049/pexels-photo-137049.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/66292/cat-eyes-view-face-66292.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/8923/pexels-photo.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/54632/cat-animal-eyes-grey-54632.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/4602/jumping-cute-playing-animals.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/137049/pexels-photo-137049.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/66292/cat-eyes-view-face-66292.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/8923/pexels-photo.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/54632/cat-animal-eyes-grey-54632.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/4602/jumping-cute-playing-animals.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/137049/pexels-photo-137049.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/66292/cat-eyes-view-face-66292.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/8923/pexels-photo.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/54632/cat-animal-eyes-grey-54632.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/4602/jumping-cute-playing-animals.jpg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/137049/pexels-photo-137049.jpeg?w=100&h=100&fit=crop&auto=compress',
  'https://images.pexels.com/photos/66292/cat-eyes-view-face-66292.jpeg?w=100&h=100&fit=crop&auto=compress',
]

const ROWS = [{},{},{},{},{},{},{},{},{},{}] // eslint-disable-line