import React from 'react';
import {View, Text,StatusBar,TouchableHighlight,StyleSheet,TouchableOpacity,BackHandler} from 'react-native';
import {Image,Spinner,Caption,Icon,DropDownMenu,Title, TextInput} from '@shoutem/ui';
import { TimingDriver, FadeIn,ScrollDriver } from '@shoutem/animation';
import { TabbedPager } from 'react-native-viewpager-carousel';
import ExamplePage from './ExamplePage';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import {loggout} from '../Actions/HomeActions';
import {dados} from '../Actions/AuthActions';
import Chave from './Initial/ChavePB';
import firebase from 'firebase';
import _ from 'lodash';
import SideMenu from 'react-native-side-menu';
import Menu from './Menu';
import Icone from 'react-native-vector-icons/MaterialCommunityIcons';


const driver = new ScrollDriver();

 class Home extends React.Component {

  constructor(props) {
    super(props)

   

    this.dataSource = []

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
    };


    this.dataSource = [...this.dataSource, {
    
        index: 1,
        title: 'Contatos',
      },
    {  
      index: 2,
      title: 'Conversas',
    }
    ]
  }
  
 
  
  componentDidMount() {
  
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
      
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillReceiveProps(nextProps){
     
    }
    handleBackButton() {
        return true;
    }
 _renderTab = ({data, _pageIndex}) => (
    <View></View>
  )

  _renderPage = ({data}) => {
    return (

      <ExamplePage
      myEmail={this.props.email_login}
      mirrorChildren={false}
      index={data.index}
      title={data.title}
      />


    )
  }


  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item =>
    this.setState({
      isOpen: false,
      selectedItem: item,
    });




  render() {
    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;

    return (
      <SideMenu
      menu={menu}
      isOpen={this.state.isOpen}
      onChange={isOpen => this.updateMenuState(isOpen)}
    >
      <View style={{backgroundColor:'white', flex:1}} >
        <StatusBar
            backgroundColor="lightgray"
            barStyle="dark-content"
          />
        <View style={{backgroundColor:'white', borderBottomColor:'lightgray', borderBottomWidth:1, height:90, padding:10,flexDirection:'row',justifyContent: 'space-between',}} >

            <View>
                      <TouchableOpacity
                    onPress={this.toggle}
                    style={styles.button}
                  >
                  
                  <Icone name="reorder-horizontal" size={30} color="gray" />
                   
                  </TouchableOpacity>
                  
               
            </View>

            <View style={{marginLeft:20}} >

            </View>

            <View style={{justifyContent:'flex-start'}} >
           
            <Text>info ?: {this.props.info}</Text>
          
                  <Text style={{color:'gray', fontSize:18}} >Meu nome</Text>
                  <Text style={{color:'gray', fontSize:12}} >Meu nome</Text>
            </View>
            <View>
                  <TouchableHighlight
                          underlayColo="transparent"
                          onPress={r => { false}}
                          >
                          <View style={{backgroundColor:'white',  padding:1,
                              borderColor:'lightgreen',
                              borderWidth:1,
                              borderBottomLeftRadius: 45,
                            borderBottomRightRadius: 45,
                            borderTopLeftRadius: 47,
                            borderTopRightRadius: 47
                          }} >

                                <Image
                                  style={{width:68,height:65}}
                                  styleName="medium-avatar"
                                    source={{ uri: 'https://thumbs.dreamstime.com/b/%C3%ADcone-do-sinal-do-usu%C3%A1rio-s%C3%ADmbolo-da-pessoa-avatar-humano-84519083.jpg'}}
                                />
                          </View>
                  </TouchableHighlight>
            </View>
        </View>
        <View style={{height:10, backgroundColor:'white', justifyContent:'center', alignItems:'center'}} >
        <TouchableHighlight
                          underlayColo="transparent"
                          onPress={r => alert('click')}
                          >
                        <Text></Text>
                  </TouchableHighlight>
        </View>

        <TabbedPager
          ref={tabbarPager => {
            this.tabbarPager = tabbarPager
          }}
          experimentalMirroring={false}
          data={this.dataSource}
          thresholdPages={0}
          renderAsCarousel={false}
          renderTab={this._renderTab}
          renderPage={this._renderPage}
          lazyrender={true}
          lazyrenderThreshold={0}
          scrollEnabled={true}
        />


      </View>
      </SideMenu>
    );
  }
}

const styles = StyleSheet.create({
  modalContent:{
    backgroundColor:'white',
    borderRadius:10,
    flex:1,
    borderWidth:2,
    padding:10,
    borderColor:'gray'

  },
  button: {
    position: 'absolute',
    top: 10,
    padding: 10,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    margin: 10,
    padding: 10,
  },
})
const  mapStateToProps =state => ({
  modal: state.HomeReducer.modal,
  logged: state.AuthReducer.isLogged,
  info: state.AuthReducer.info,
  email_login: state.AuthReducer.email
})

export default connect(mapStateToProps,{loggout})(Home);
