import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,StatusBar,ListView,
  StyleSheet,ActivityIndicator,BackHandler
} from 'react-native';
import SearchBar from 'react-native-searchbar';
import axios from 'axios';
import People from './Search/People';
import {connect} from 'react-redux';
import {alterarLoad, searchUser,verificaLoad} from '../Actions/HomeActions';
import { Actions } from 'react-native-router-flux';

const ListagemItesns = [] ;

class SearchContact extends React.Component {

  constructor(props) {
    super(props);
  
  
   
    
    
  }

  componentDidMount() {

   
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {   
      Actions.pop();   
        return true;
    }

  _handleResults(results) {  
    
        
    
    
        
    }
  
    _renderRow(contato){
      return(
        <TouchableHighlight
          onPress={() => {false}}
        >
        <View style={{flex:1, borderBottomWidth:1, borderColor:'#CCC', padding:20}}>      
         
        </View>
        </TouchableHighlight>
      );
    }
    
  

            _load(){
                           
                    if(this.props.loadingSearchuser){
                        return(
                            <View style={{ alignItems:'center', marginTop:80, justifyContent: 'flex-end'}}>
                            
                            
                            <ActivityIndicator size="large" color="gray" />
                            
                          </View>
                          );
                    }
             
                 
                return(     
                  <ScrollView style={styles.container} >
                 
              <People  dados={this.props.users_encontrados}  />
       
              </ScrollView>
                );  
                        
            
              
            }
  

  render() {
    return (
      <View style={{backgroundColor:'white', flex:1}} >
       <StatusBar
            backgroundColor="lightgray"
            barStyle="dark-content"
          />
        <View style={{ marginTop: 35 ,backgroundColor:'white'}}>
        {this._load()}
      
        
        </View>
        <SearchBar
          ref={(ref) =>{ this.searchBar = ref;}}
          data={ListagemItesns}
          handleResults={this._handleResults}
          showOnLoad
          onBack={voltar => { Actions.pop() } }
          placeholder="Buscar"          
          handleChangeText={val =>{
            this.props.searchUser(val);
            this.props.verificaLoad(val);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      paddingTop: 25,
      paddingLeft:5,
      backgroundColor: 'white'
          
    },
  });
  
  const mapStateToProps = state =>({
      load: state.HomeReducer.load,
      naoEncontrou: state.HomeReducer.naoEncontrou,
      users_encontrados: state.HomeReducer.users_encontrados,
      loadingSearchuser: state.HomeReducer.loadingSearchuser
  });

  export default connect(mapStateToProps,{alterarLoad,verificaLoad,searchUser})(SearchContact);