import React from 'react';
import {View, Text } from 'react-native'
import Home from './Home';

import firebase from 'firebase';
import {connect} from 'react-redux';
import { verificaUsuario } from '../Actions/AuthActions';

import Inicio from './Initial/Initial';
import Sign from './SignIn/SignIn';
import Search from './SearchContact';

 class AutenticaUsuario extends React.Component{
    constructor(props){
        super(props);

    }
    componentWillMount(){
        this.props.verificaUsuario()
        
    }

    componentDidMount(){

    }

    _verificaFinal(){
        if(this.props.logged){ 
           
                return( <Home />);
           
            }
        if(this.props.logged == false){
            
                return( <Inicio /> ); 
            
              }        
        return( <View style={{backgroundColor:'white', flex:1, justifyContent:'center', alignItems:'center'}} ><Text>CARREGANDO</Text></View> );
    }

    render(){
        return(this._verificaFinal());
    }
}
const mapStateToProps = state => ({
    logged: state.AuthReducer.logged,
    statusUser: state.AuthReducer.statusUser
})
export default connect(mapStateToProps,{verificaUsuario})(AutenticaUsuario);
