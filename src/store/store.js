import { applyMiddleware, createStore, compose } from 'redux';

import ReduxThunk from 'redux-thunk';
import rootReducer from '../Reducers';
import { persistStore, persistReducer} from 'redux-persist'



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = [ReduxThunk];


export default () => {
  let store = createStore(rootReducer , {},composeEnhancers(applyMiddleware(...middlewares)) )
  let persistor = persistStore(store)
  return { store, persistor }
}
