import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import HomeReducer from './HomeReducer';
import ListarReducerSolicitacao from './ListarReducerSolicitacao';
import ListarReducerContatos from './ListarReducerContatos'
import {  persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage' // or whatever storage you are using
import {AsyncStorage} from 'react-native'
import createSensitiveStorage  from 'react-native-sensitive-info'
import MessagesReducer from './MessagesReducer'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';





const persistConfigAll = {
  key: 'all',
  storage:AsyncStorage,  
  stateReconciler:autoMergeLevel2,
  blacklist:['ListarReducerContatos','ListarReducerSolicitacao','authUser','MessagesReducer']
}

const persistConfig = {
    key: 'auth',
    storage:AsyncStorage,
    stateReconciler:autoMergeLevel2,
   
  }

  const persistConfig2 = {
    key: 'home',
    storage:AsyncStorage,
    stateReconciler:autoMergeLevel2,
    
    
  }

  
  const rootReducer = combineReducers({
    AuthReducer,   
   HomeReducer,
   ListarReducerSolicitacao: ListarReducerContatos,
   ListarReducerContatos,
   MessagesReducer
  })
  
  export default  persistReducer( persistConfigAll,rootReducer)
  