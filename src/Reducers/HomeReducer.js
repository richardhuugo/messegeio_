const INITIAL_STATE =
{
    estado_online: '',
    modal:3,
    load:false,
    naoEncontrou:false,
    loadingSearchuser:false,
    users_encontrados:[],
    usuario_final:null,
    estado:"",
    viewProfile:false,
    load_privacidade:false,
    loadChangePrivacidade:false,
    user_solicitacoes:null,
    teste:'',
    meuContato:false


}
import {CADASTRO_USUARIO,
        ERRO_CADASTRO,
        MODIFICAR_EMAIL,
        LOGGED,VIEW_PROFILE,
        USER_FETCH_SOLICITACOES,
        MODIFICAR_LOAD,
        
        FECHAR_MODAL,
        BUSCANDO_USER,
        USER_SEARCH,
        DADOS_PRIVACIDADE,
        LOAD_PRIVACIDADE,
        MODIFICAR_SENHA,
    ALT_EMAIL_LOGIN, LOGIN, ALT_SENHA_LOGIN} from '../Actions/types';

export default (state = INITIAL_STATE, action) =>{
    switch(action.type){
        case 'meucontato':
        return{...state, meuContato:action.payload}
        case 'teste'    :
            return{...state, teste:action.payload}
        case LOAD_PRIVACIDADE:
            return{...state, load_privacidade:action.load_privacidade }
        case DADOS_PRIVACIDADE:
            return{...state, viewProfile:action.viewProfile}
        case VIEW_PROFILE:
            return{...state, viewProfile:action.payload}
        case "adicionou":
            return{...state, estado:action.payload}
        case "verifica":
            return{...state,usuario_final:action.usuario_final }
        case BUSCANDO_USER:
            return{...state, loadingSearchuser:action.loadingSearchuser}
        case USER_SEARCH:
            return{...state, users_encontrados: action.users_encontrados}
       
        case MODIFICAR_LOAD:
            return{...state, load:action.payload}
        case FECHAR_MODAL:
            return{...state, modal:action.payload}
        default:
            return state;
    }
}
