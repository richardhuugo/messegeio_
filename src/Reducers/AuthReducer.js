const INITIAL_STATE = 
{
    email: null,    
    senha:'',    
    nome_cadastro:'',   
    pin_cadastro:'',
    cadastro_mensagem:'',
    senha_pb:'',
    load_cadastro:false,
    load_login:false,
    logged:null,
    statusUser: null,
    mensagem4:'',
    user_name:'',
    disponibilidade:null, 
    isLogged: false,
    privateKey:null,

    
    
}
import {CADASTRO_USUARIO,
        ERRO_CADASTRO,
        MODIFICAR_EMAIL,NOME_CADASTRO,
        LOGGED,SENHA_PUBLICA_USUARIO,        
        MODIFICAR_SENHA,
        MODIFICAR_PIN,
        USER_NAME_CADASTRO,
        VERIFICA_DISPONIBILIDADE,
    ALT_EMAIL_LOGIN, LOGIN, ALT_SENHA_LOGIN} from '../Actions/types';

export default (state = INITIAL_STATE, action) =>{
    switch(action.type){   
        case 'PRIVATE/KEY':
            return{...state, privateKey: action.privateKey}       
        case VERIFICA_DISPONIBILIDADE:
            return {...state, disponibilidade:action.disponibilidade}
        case USER_NAME_CADASTRO:
            return {...state, user_name:action.payload }
        case MODIFICAR_PIN:
            return{...state, pin_cadastro:action.payload}
        case 'mensagem4':
            return{...state, mensagem4:action.payload}
        case NOME_CADASTRO:
            return{...state, nome_cadastro:action.payload}
        case SENHA_PUBLICA_USUARIO:
            return{...state, senha_pb:action.payload}
        case LOGGED:
            return{...state, logged:action.payload ,statusUser:action.statusUser}
        case LOGIN:
            return{...state,  load_login:action.loadLogin}
        case ALT_EMAIL_LOGIN:
            return {...state, email:action.payload}
        case ALT_SENHA_LOGIN:
            return {...state, senha: action.payload}
        case MODIFICAR_EMAIL:
            return {...state, email:action.payload}
        case MODIFICAR_SENHA:
            return {...state, senha:action.payload}
        case CADASTRO_USUARIO:
            return {...state,  load_cadastro:action.load}
        case ERRO_CADASTRO:
            return {...state, senha:'', cadastro_mensagem: action.cadastro_mensagem, load_cadastro:action.load}
        case 'LOGGOUT/SAIR':
            return{...state, email_login: null,    
                senha_login:'',    
                nome_cadastro:'',
                email_cadastro:'',
                senha_cadastro:'',
                pin_cadastro:'',
                cadastro_mensagem:'',
                senha_pb:'',
                load_cadastro:false,
                load_login:false,
                logged:null,
                statusUser: null,
                mensagem4:'',
                user_name:'',
                disponibilidade:null, 
                isLogged: false, }
        default:
            return state;
    }
}