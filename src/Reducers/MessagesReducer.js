const INITIAL_STATE = {
    keyMessage:null,
    mensagemSend:''
    
}

export default (state = INITIAL_STATE, action) =>{
    switch(action.type){   
        case 'TO/MESSAGE':
            return {...state, keyMessage: action.keyMessage}
        case 'MESSAGE/SEND':
            return{...state, mensagemSend:action.payload}
        default:
            return state;
    }
}